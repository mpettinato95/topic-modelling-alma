# Set up the environment for parsing and managing the log.
# Please set classpath to your AlmaParser bin folder.
# Use parse(xml) to parse an xml file. 
# The functions are just shortcuts.
#
# author Michele Pettinato

# Set this to your actual logParser classpath
classpath <- "logParser/bin"

options(java.parameters = "- Xmx4096m")
library(rJava)
.jinit()
.jaddClassPath(classpath)
rpu <- .jnew("rparseutility/RParseUtility")
flt <- .jcall(rpu, "Lrparseutility/Selector;", "getFilter")
sel <- .jcall(rpu, "Lrparseutility/Projector;", "getSelect")
src <- .jcall(rpu, "Lrparseutility/ParseSelector;", "getSearch")
mem <- .jcall(rpu, "Lrparseutility/Memory;", "getMemory")

# functions for parsing
search <- function(method, ...) .jcall(src, "V", method, ...)					# e.g.: search("forContainer", <ContainersName>)
parse <- function(autoskip=FALSE, ...) .jcall(rpu, "V", "parse", autoskip, ...)			# e.g.: parse(<FileOrFolder>)
parseFailures <- function(autoskip=FALSE, ...) .jcall(rpu, "V", "parseFailures", autoskip, ...)	# fix exceptions.

# functions for filtering the data
where <- function(method, ..., remove=FALSE) {							# e.g.: where("type", "error")
	if (remove) {
		.jcall(flt, "V", "containerWithRemove", .jarray(c(...)))
	} else {
		.jcall(flt, "V", method, .jarray(c(...)))
	}
}		
reset <- function() {										# reset all filters
	.jcall(mem, "V", "reset")	
	.jcall(src, "V", "reset")
}		
rewind <- function() .jcall(mem, "V", "rewind")
mark <- function() .jcall(mem, "V", "mark")			

# functions for selecting fields
get <- function(field)	.jcall(rpu, "I", field)							# e.g.: get("count")
select <- function(field, ...)	.jcall(sel, "[S", field, ...)					# e.g.: select("timestamp")

# functions for managing the memory
clear <- function() .jcall(mem, "V", "clear")							# clear the memory.
savestate <- function(name) .jcall(mem, "V", "saveState", paste(getwd(), "/", name, sep=""))
loadstate <- function(name) .jcall(mem, "V", "loadState", paste(getwd(), "/", name, sep=""))
apply_filters <- function() .jcall(mem, "V", "apply")						# permanently apply the filters
from <- function(..., message=TRUE) .jcall(mem, "V", "from", ..., message) 			# where to fetch the fields from.
												# from("SYSTEM") or from("DOCUMENT")
