Repository of logs: "http://computing-logs.aiv.alma.cl/index.php?dir=" under the directory "APE1/SYSTEM"

Below every page there is a button to download a folder of logs (you may want to download a day)
A day of logs is about 1.6 GB.

--------------------------------------------------------------------------------------------------------------------------

Here is an example of usage of this program 

Please set R's working directory to the 'scripts' folder.

1) Log parsing:
	source("logParser.r")
	search("forContainer", "CONTROL/ACC/javaContainer")
	parse(logFolder)

   or if you want to skip log parsing I have provided a file with already parsed logs
	source("logParser.r")
	loadstate("states/2017-03-13_testbed") 	

2) Data preparation and Training:
	source("data.r")
	generateDocs()
	from("docs")
	get("count") 					# count the size of the corpus
	lda <- LDA(preProcess(select("message")), k=9)  # train the LDA model

3) Log Analysis:
	source("mapping.r")
	logMap <- assignToTopics(lda, k=9)		# log mapping
	unnameLogMap(logMap[[1]][[1]])			# message inspection of the first topic in the first sequence
	colorSequences(logMap)				# check the output file "coloredSequences.txt" (to copy in an Excel file)

logMap is a sequence x topic x log message array. Each log message is an object with 2 attributes: '$event' and '$dist'.
The former is the log message string, while the latter is the distribution of topics of the message. unnameLogMap()

------------------------------------------------------------------------------------------------------------------------

Important commands explained:
1) in R, source("logParser.r") to set up the java environment:
	Please check the 'classpath' variable in logParser.r is set to the logParser bin folder.
	- Use search(forAttribute, value) to specify what information to parse in log files.
	- Use parse(directory) to parse an xml file or folder (xml files must be compressed in zip).
	- Use select(attribute) to project an attribute of an xml node
	- Use where(attribute, value) to filter parsed logs in memory by attribute (regex are also possible)
	- Use mark() - reset() - rewind() to save, load or clear filters respectively
2) source("data.r") to prepare the data:
	- Use generateDocs() to spawn a corpus of documents
	- Use from("DOCUMENT") to specify that the query refers to the list of documents resulting
		from the generateDocs() function.
		from("SYSTEM") specifies that the query refers to the list of system logs resulting
		from the the parse() function.
	- Use preProcess(select("message")) to preprocess the log messages.
	- Use LDA(processedDocs, k=9, ...) to train an LDA model.
		- Use posterior(lda)$terms or posterior(lda)$topics to inspect the model.
		- Use posterior(lda, message) to test the model on a message.
	- Use savestate("states/..") - loadstate("states/..") to save and load the lists of
		system logs AND documents.
3) source("mapping.r") for data analysis
	- Use assignToTopics(lda, k=9) for log mapping
		A logMap is an array Sequences x Topics x Log messages
	- Use colorSequences(logMap) to color sequences where logMap is the output of assignToTopics().
		The output is the file "coloredSequences.txt" (copy the content in an excel 
		file to have a better look).
	- Use save.image() to save the LDA model, log mapping and sequence coloring.

-----------------------------------------------------------------------------------------------------------------------


The author ~ Michele Pettinato (michele.pettinato@epfl.ch)