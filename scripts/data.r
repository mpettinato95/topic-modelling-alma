library("topicmodels")
#library("tm")
library(quanteda)

preProcess <- function(msg, print=TRUE) {
	for (i in seq(msg)) {
		msg[[i]] <- gsub("[^ ^:^(^)]+/[^ ^:^(^)]*", "ldapathplaceholder", msg[[i]])
		msg[[i]] <- gsub("\\d+\\.\\d+[Gg][Hh]z", "ghz", msg[[i]])
	}
	if (print) {
		print("Paths removed")
		print("GHz unified")
	}
	res <- dfm(msg, verbose=print, toLower=TRUE, removeNumbers=TRUE, removePunct=TRUE, removeTwitter=TRUE, stem=TRUE, ignoredFeatures = stopwords("english"), language="english")
	# StackOverflow, to remove empty documents
	oldLength <- nrow(res)
	rowTotals <- apply(res , 1, sum)
	res[rowTotals> 0, ] -> res
	newLength <- nrow(res)
	if (newLength < oldLength) {
		print(paste("WARNING:", oldLength - newLength, "documents removed"))
	}
	#print(res)
	res
}


generateDocs <- function() {
	from("system", message=FALSE)
	where("container", "CONTROL/ACC/javaContainer")
	mark()
	comps <- names(table(select("component")))
	print(comps)
	for (i in seq(comps)) {
		print(paste("Processing", comps[[i]]))
		where("component", comps[[i]])
		.jcall("rparseutility/Document", "V", "appendComponent", mem)
		reset()
	}
	rewind()
}


