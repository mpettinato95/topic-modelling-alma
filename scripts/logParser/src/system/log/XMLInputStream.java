package system.log;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class XMLInputStream extends InputStream {

	private static final byte DEFAULT_CHARACTER = ' ';
	private boolean EOF = false;
	private byte[] root;
	private boolean firstStream;
	private int read_count;
	private GZIPInputStream gzip_is;

	public XMLInputStream(String file) throws IOException {
		root = "Log".getBytes();
		firstStream = true;
		read_count = 0;
		gzip_is = new GZIPInputStream(new FileInputStream(file));
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (EOF) {
			return -1;
		}
		if (firstStream) {
			firstStream = false;
			for (int i = 0; i < read_count; i++) {
				if (b[i] == '>') {
					throw new IOException("An unexpected root has been acknowledged.");
				}
			}
			if (b[1] != '!' && b[1] != '?' && (b[1] != 'L' || b[2] != 'o'
					|| b[3] != 'g')) {
				return openRootTag(b, off, len);
			}
		}
		return readAndStrip(b, off, len);
	}

	private int readAndStrip(byte[] b, int off, int len) throws IOException {
		int result = gzip_is.read(b, off, len);
//		System.out.println(new String(b));
		if (result < 0) {
			result = closeRootTag(b, off);
			gzip_is.close();
			EOF = true;
		} else {
			for (int i = 0; i < result; i++) {
				if (!isValidXMLCharacter(b[i])) {
					b[i] = DEFAULT_CHARACTER;
				}
			}
		}
		return result;
	}

	private int closeRootTag(byte[] b, int off) {
		b[0] = '<';
		b[1] = '/';
		for (int i = 0; i < root.length; i++) {
			b[i + 2] = root[i];
		}
		b[root.length + 2] = '>';
		return root.length + 3;
	}

	private int openRootTag(byte[] b, int off, int len) throws IOException {
		byte write = '>', temp;
		 if (read_count >= b.length)
			 throw new IOException("The buffer is full, cannot inject the dummy root.");
		for (int i = 0; i <= read_count; i++) {
			temp = b[i];
			b[i] = write;
			write = temp;
		}
		root = new byte[read_count - 1];
		for (int i = 2; i <= read_count; i++) {
			root[i - 2] = b[i];
		}
		return read_count + 1;
	}

	/**
	 * This method ensures that the output String has only valid XML unicode
	 * characters as specified by the XML 1.0 standard. For reference, please
	 * see <a href="http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char">the
	 * standard</a>. This method will return an empty String if the input is
	 * null or empty.
	 *
	 * @param in
	 *            The String whose non-valid characters we want to remove.
	 * @return The in String, stripped of non-valid characters.
	 */
	public boolean isValidXMLCharacter(int current) {
		return (current == 0x9) || (current == 0xA) || (current == 0xD)
				|| ((current >= 0x20) && (current <= 0xD7FF))
				|| ((current >= 0xE000) && (current <= 0xFFFD))
				|| ((current >= 0x10000) && (current <= 0x10FFFF));
	}
	
	@Override
	public int read(byte[] b) {
		throw new RuntimeException("Unimplemented read method.");
	}

	@Override
	public int read() throws IOException {
		read_count++;
		return gzip_is.read();
	}
	
	@Override
	public void close() throws IOException {
		gzip_is.close();
	}
}
