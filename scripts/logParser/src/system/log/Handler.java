package system.log;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import system.event.Data;
import system.event.SystemEvent;

/**
 * The Handler utilized by the parser. This handler parses a single file. It is
 * responsible for collecting the events, assigning them the right ordinal
 * (based on the log's list size) and finally adding them to the log if the
 * operation was successful. Otherwise, any partial result is discarded and the
 * log is unaffected.
 * 
 * @author Michele
 *
 */
public class Handler extends DefaultHandler {

	/*
	 * A temporary list of events. This will be appended to the log's one.
	 */
	private final ArrayList<SystemEvent> list;
	private SystemEvent currentEvent; // Holds the event object being
										// initialized
	// between the tags of that event.
	private Data currentData; // Holds the data object being initialized between
								// the tags <Data> and </Data>.
	private boolean withinEvent; /*
								 * The stream is currently between <SomeEvent>
								 * and </SomeEvent> tags (SomeEvent is a
								 * placeholder)
								 */
	private boolean withinData; // The stream is currently between <Data> and
								// </Data> tags
	/*
	 * After the parse was successful, the obtained list of events is appended
	 * to this log.
	 */
	private final SystemLog log;

	/*
	 * This value, initialized to the log list size, is used to set the ordinal
	 * of the parsed events.
	 */
	private int ordinal;

	/**
	 * Object initialization.
	 * 
	 * @param log
	 *            The events obtained by the parse will be appended to this
	 *            log's list only in case of success. In case of failure this
	 *            log is unaltered
	 */
	public Handler(SystemLog log) {
		withinEvent = false;
		withinData = false;
		list = new ArrayList<>();
		this.log = log;
		ordinal = log.getEvents().size();
	}

	@Override
	/**
	 * Analyze a tag. If that is a log entry type, create a new SystemEvent. 
	 * If it is not 
	 */
	public void startElement(String namespaceURI, String localName,
			String qName, Attributes atts) throws SAXException {
		LogEntryType entryType = LogEntryType.getByName(qName);
		if (entryType == null) {
			// The tag is not an event, perhaps a data.
			if (withinEvent && qName.equals("Data")) {
				currentData = new Data(atts);
				currentEvent.addData(currentData);
				withinData = true;
			}
		} else if (areRelevant(atts)) {
			try {
				currentEvent = new SystemEvent(entryType, ++ordinal, atts);
				list.add(currentEvent);
				withinEvent = true;
			} catch (IllegalArgumentException iae) {
				iae.printStackTrace();
			}
		}
	}

	/*
	 * Return whether these attributes are relevant.
	 */
	private boolean areRelevant(Attributes atts) {
		if (log.getContainers().size() == 0)
			return true;
		String val = atts.getValue("Process");
		// System.out.println(val);
		for (String container : log.getContainers()) {
			if (container.equals(val)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (withinData) {
			withinData = false;
			currentData = null;
		} else if (withinEvent) {
			withinEvent = false;
			currentEvent = null;
		}
	}

	@Override
	public void endDocument() {
		log.getEvents().addAll(list);
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if (withinData) {
			currentData.setValue(new String(ch, start, length));
		} else if (withinEvent) {
			currentEvent.setMessage(new String(ch, start, length));
		}
	}
}
