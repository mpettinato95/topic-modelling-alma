package system.log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import alma.Log;
import alma.ParseException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import system.event.Data;
import system.event.SystemEvent;
import system.event.RCInformation;
import system.event.SCInformation;

/**
 * 
 * @author Michele
 *
 */
public class SystemLog extends Log<SystemEvent> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8569741280772749718L;

	private transient SAXParser saxParser;

	/**
	 * Limit the parse only to these containers.
	 */
	private transient HashSet<String> containers;

	public SystemLog() throws ParseException {
		super();
	}

	@Override
	protected void parseProcedure(String filename) throws ParseException {
		try {
			InputSource is = new InputSource(new XMLInputStream(filename));
			Handler handler = new Handler(this);
			saxParser.parse(is, handler);
		} catch (SAXException | IOException e) {
			throw new ParseException(e);
		}
	}

	/**
	 * Get all the events of the specified level. Use this method to apply a
	 * filter on an already filtered set of events.
	 * 
	 * @param evnts
	 *            the array of events of different levels
	 * @param types
	 *            only the events with this types will be selected
	 * @return an ArrayList with all the events with the specified level or an
	 *         empty ArrayList if none exists.
	 */
	public static ArrayList<SystemEvent> getByType(
			ArrayList<SystemEvent> evnts, String... types) {
		ArrayList<SystemEvent> result = new ArrayList<>();
		for (int i = 0; i < types.length; i++) {
			types[i] = types[i].toUpperCase();
		}
		Pattern[] patterns = compilePatterns(types);
		for (SystemEvent e : evnts) {
			String type = e.getType().toString();
			if (matches(type, patterns)) {
				result.add(e);
			}
		}
		return result;
	}

	/**
	 * Does the string matches one of the patterns?
	 * 
	 * @param string
	 *            the string to be matched
	 * @param find
	 *            whether a substring or the whole string should be matched.
	 * @param patterns
	 *            the patterns one is searching for.
	 * @return
	 */
	public static boolean matches(String string, Pattern[] patterns) {
		if (string == null)
			return false;
		for (Pattern pattern : patterns) {
			Matcher matcher = pattern.matcher(string);
			if (matcher.find()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get all the events for the specified audience. Use this method to apply a
	 * filter on an already filtered set of events.
	 * 
	 * @param evnts
	 *            the array of events containing logs for different audiences.
	 * @param audience
	 *            only the events for this audience will be selected
	 * @return an ArrayList with all the events for the specified audience or an
	 *         empty ArrayList if none exists.
	 */
	public static ArrayList<SystemEvent> getByAudience(
			ArrayList<SystemEvent> systemEvents, String... audiences) {
		ArrayList<SystemEvent> result = new ArrayList<>();
		Pattern[] patterns = compilePatterns(audiences);
		for (SystemEvent e : systemEvents) {
			if (matches(e.getAudience(), patterns)) {
				result.add(e);
			}
		}
		return result;
	}

	public static ArrayList<SystemEvent> getByFile(
			ArrayList<SystemEvent> systemEvents, String... files) {
		ArrayList<SystemEvent> result = new ArrayList<>();
		Pattern[] patterns = compilePatterns(files);
		for (SystemEvent e : systemEvents) {
			String file = e.getFile();
			if (matches(file, patterns)) {
				result.add(e);
			}
		}
		return result;
	}

	public static ArrayList<SystemEvent> getByContainer(
			ArrayList<SystemEvent> systemEvents, String... containers) {
		ArrayList<SystemEvent> result = new ArrayList<>();
		Pattern[] patterns = compilePatterns(containers);
		for (SystemEvent e : systemEvents) {
			String process = e.getProcess();
			if (matches(process, patterns)) {
				result.add(e);
			}
		}
		return result;
	}
	
	public ArrayList<SystemEvent> getByContainerAndRemove(
			ArrayList<SystemEvent> systemEvents, String... containers) {
		System.out.println("<Debug> getByContainerAndRemove1");
		ArrayList<SystemEvent> result = getByContainer(systemEvents, containers);
		System.out.println("<Debug> getByContainerAndRemove2");
		events.removeAll(result);
		System.out.println("<Debug> getByContainerAndRemove3");
		for (int i = result.get(0).getOrdinal() - 1; i < events.size(); i++) {
			events.get(i).setOrdinal(i + 1);
		}
		System.out.println("<Debug> getByContainerAndRemove4");
		return result;
	}

	public static ArrayList<SystemEvent> getByMessage(
			ArrayList<SystemEvent> systemEvents, String[] regex) {
		ArrayList<SystemEvent> result = new ArrayList<>();
		for (int i = 0; i < regex.length; i++) {
			regex[i] = regex[i].toLowerCase();
		}
		Pattern[] patterns = compilePatterns(regex);
		for (SystemEvent e : systemEvents) {
			try {
				String message = e.getMessage().toLowerCase();
				if (matches(message, patterns)) {
					result.add(e);
				}
			} catch (NullPointerException exc) {
				// only toLowerCase can throw the NullPointerException
			}
		}
		return result;
	}

	public static ArrayList<SystemEvent> getByData(
			ArrayList<SystemEvent> systemEvents, String[] regex) {
		ArrayList<SystemEvent> result = new ArrayList<>();
		for (int i = 0; i < regex.length; i++) {
			// this will probably screw some regex.
			regex[i] = regex[i].toLowerCase();							
		}
		Pattern[] patterns = compilePatterns(regex);
		for (SystemEvent e : systemEvents) {
			try {
				ArrayList<Data> data = e.getData();
				String eventData = "";
				for (Data d : data) {
						eventData += d.toString().toLowerCase() + " ";
				}
				if (matches(eventData, patterns)) {
					result.add(e);
				}
			} catch (NullPointerException exc) {
				// only toString can throw the NullPointerException
			}
		}
		return result;
	}

	public static Pattern[] compilePatterns(String[] set) {
		Pattern[] patterns = new Pattern[set.length];
		for (int i = 0; i < set.length; i++) {
			patterns[i] = Pattern.compile(set[i]);
		}
		return patterns;
	}

	/**
	 * Output to file the events.
	 * 
	 * @param directory
	 *            the output file.
	 * @throws FileNotFoundException
	 */
	public void print(String directory) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(directory);
		writer.println("total: " + events.size());
		writer.println(String.format(SystemEvent.formatStr, "Level",
				"Timestamp", "LogId", "Priority", "Audience", "Data"));
		for (SystemEvent e : events) {
			writer.println(e.toString());
		}
		writer.close();
	}

//	/**
//	 * Output to file the runtime context information of events.
//	 * 
//	 * @param directory
//	 *            the output file.
//	 * @throws FileNotFoundException
//	 */
//	public void printRCInformation(String directory)
//			throws FileNotFoundException {
//		PrintWriter writer = new PrintWriter(directory);
//		writer.println(String.format(RCInformation.formatstr, "Host",
//				"Process", "Thread", "SourceObject"));
//		for (SystemEvent e : events) {
//			writer.println(e.rciT);
//		}
//		writer.close();
//	}
//
//	/**
//	 * Output to file the source code information of events.
//	 * 
//	 * @param directory
//	 *            the output file.
//	 * @throws FileNotFoundException
//	 */
//	public void printSCInformation(String directory)
//			throws FileNotFoundException {
//		PrintWriter writer = new PrintWriter(directory);
//		writer.println(String.format(SCInformation.formatstr, "File", "Line",
//				"Routine"));
//		for (SystemEvent e : events) {
//			writer.println(e.getSCInformation().toString());
//		}
//		writer.close();
//	}

	@Override
	public String toString() {
		String string = "Event set:\n";
		int limit = 0;
		string += "LogId\tUri\tLevel\tTimestamp\n";
		for (SystemEvent e : events) {
			string += e.toString() + "\n";
			limit++;
			if (limit == MAX_ROWS)
				break; // Since java concatenation is rather slow, break after
						// some time.
		}
		return string;
	}

	public int getDataCount() {
		int count = 0;
		for (SystemEvent e : events) {
			count += e.getData().size();
		}
		return count;
	}

	@Override
	public void init() throws ParseException {
		containers = new HashSet<>();
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			saxParser = spf.newSAXParser();
		} catch (ParserConfigurationException | SAXException e) {
			throw new ParseException(e);
		}
	}

	public void setEvents(ArrayList<SystemEvent> systemEvents) {
		events.clear();
		events.addAll(systemEvents);
	}

	public HashSet<String> getContainers() {
		return containers;
	}

	public static ArrayList<SystemEvent> getByOrdinal(
			ArrayList<SystemEvent> systemEvents, int lowerBound, int upperBound) {
		return new ArrayList<SystemEvent>(systemEvents.subList(lowerBound - 1,
				upperBound));
	}

	public static ArrayList<SystemEvent> getByComponent(
			ArrayList<SystemEvent> systemEvents, String[] components) {
		ArrayList<SystemEvent> result = new ArrayList<>();
		Pattern[] patterns = compilePatterns(components);
		for (SystemEvent e : systemEvents) {
			String component = e.getComponent();
			if (matches(component, patterns)) {
				result.add(e);
			}
		}
		return result;
	}

	public static ArrayList<SystemEvent> getByThread(
			ArrayList<SystemEvent> systemEvents, String[] threads) {
		ArrayList<SystemEvent> result = new ArrayList<>();
		Pattern[] patterns = compilePatterns(threads);
		for (SystemEvent e : systemEvents) {
			String component = e.getThread();
			if (matches(component, patterns)) {
				result.add(e);
			}
		}
		return result;
	}

}
