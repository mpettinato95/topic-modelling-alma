package system.event;

import java.util.ArrayList;

import org.xml.sax.Attributes;

import alma.DataSupplier;
import alma.Event;
import system.log.LogEntryType;

public class SystemEvent extends Event implements DataSupplier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4083198701961228723L;

	/**
	 * The log entries exist in different types to distinguish between the
	 * importance of information it provides. These types are described in the
	 * following sections and follow the convention specified by $
	 * (ACE_ROOT)/ace/Log_Priority.h . Note that LogEntryType is not actually an
	 * XML tag, but merely a placeholder for the actual XML tag name such as
	 * Debug described in the following sections.
	 **/
	private LogEntryType logEntryType;

	/**
	 * The element representing a log entry is equipped with these attributes
	 * that convey the location in the source code from which the log entry was
	 * generated
	 **/
	private SCInformation sourceInfo;

	/**
	 * The log element has six attributes that give more information regarding
	 * the runtime context in which the log entry is submitted
	 **/
	private RCInformation runtimeInfo;

	/**
	 * Uniquely identifies the log entry痴 class. LogId is particularly useful
	 * when used with error messages where it can be used as a key in the help
	 * system to look up detailed help/troubleshooting information. Variable
	 * subｭelements are also useful in such cases to provide more details about
	 * the source of the log entry.
	 **/
	private int logID;

	/**
	 * The log type implies default priority of a log message. However, if the
	 * priority is explicitly specified, then the default is overridden.
	 * Priority is measured as an integer number ranging from 1 to 15, where 1
	 * is lowest and 15 highest priority. The value of 0 indicates the default
	 * priority.
	 */
	private int priority = 0;

	/**
	 * Every log entry can contain arbitrary number of <Data> subｭelements.
	 * These subｭelements are useful for reporting values of individual
	 * variables to report the state of the object that submitted the log entry.
	 * The Name attribute is mandatory as well as the content of the element.
	 */
	private ArrayList<Data> data;

	/**
	 * Optional 殿udience� set for individual logs. It can be used for
	 * filtering of logs, orthogonal to log level s. Mainly 徹PERATOR� used by
	 * Alma
	 */
	private String audience;

	/**
	 * In addition to LogId , an optional Uri attribute is provided, which
	 * uniquely identifies the log entry痴 class. (e.g.,
	 * log://www.eso.org/acs/errors/OutOfMemory ).
	 */
	private String uri;
	
	/**
	 * Column format used for the toString method. 
	 */
	public static final String formatStr = "%-10s %-25s %-15s %-10s %-15s %-60s";

	public SystemEvent(LogEntryType lev, int ordinal, Attributes atts) {
		super(atts.getValue("TimeStamp"), ordinal);
		logEntryType = lev;
		sourceInfo = new SCInformation(atts);
		runtimeInfo = new RCInformation(atts);
		audience = atts.getValue("Audience");
		uri = atts.getValue("Uri");
		message = "";
		data = new ArrayList<>();
		try {
			priority = Integer.parseInt(atts.getValue("Priority"));
		} catch (Exception e) {
		}
		try {
			logID = Integer.parseInt(atts.getValue("LogId"));
		} catch (Exception e) {
		}
	}
	
	/**
	 * Add a Data object to this event. An Event can have as many Data object as possible
	 * @param data the data object to be added.
	 */
	public void addData(Data data) {
		this.data.add(data);
	}
	
	public ArrayList<Data> getData() {
		return data;
	}
	
	public int getLogID() {
		return logID;
	}

	public String getType() {
		return logEntryType.toString();
	}

	public String getAudience() {
		return audience;
	}

	public String getUri() {
		return uri;
	}

	public String toString() {
		String time = sdf.format(timeStamp.getTime());
		return String.format(formatStr, logEntryType, time, logID, priority, 
				audience, data.toString());
	}

	@Override
	public String getFile() {
		return sourceInfo.getFile();
	}
	
	public String getRoutine() {
		return sourceInfo.getRoutine();
	}
	
	public int getLine() {
		return sourceInfo.getLine();
	}

	@Override
	public String getProcess() {
		return runtimeInfo.getProcess();
	}

	@Override
	public String getComponent() {
		return runtimeInfo.getComponent();
	}

	@Override
	public String getHost() {
		return runtimeInfo.getHost();
	}
	
	public String getThread() {
		return runtimeInfo.getThread();
	}
}
