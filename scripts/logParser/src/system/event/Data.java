package system.event;

import java.io.Serializable;

import org.xml.sax.Attributes;

public class Data implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 303368601433043615L;

	/**
	 * Mandatory attribute.
	 */
	private String name;

	/**
	 * Mandatory attribute.
	 */
	private String value;

	public Data(Attributes atts) {
		name = atts.getValue("", "Name");
	}

	public void setValue(String d) {
		value = d;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return // The data SHOULD NOT have "<" or ">" according to the
				// documentation. This commented line is
				// just for extra safety, since I saw a couple of messages
				// containing them.
				// String value = d.getValue().replaceAll("[<>]", "");
		name + " <" + value + ">";
	}
}
