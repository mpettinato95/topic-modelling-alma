package system.event;

import java.io.Serializable;

import org.xml.sax.Attributes;

/**
 * The element representing a log entry is equipped with these attributes that
 * convey the location in the source code from which the log entry was generated.
 * 
 * @author Michele
 *
 */
public class SCInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -166840946458561193L;

	/**
	 * The format of the columns used by the toString() method.
	 */
	public static final String formatstr = "%-70s %-8s %-100s";

	/**
	 * The identification of the source file. The identification should be such
	 * that it uniquely identifies the file, and that it is possible to locate
	 * the source file with only little external information (such as project�s
	 * root directory). The file name is specified relative to the root of the
	 * file system where the source file resided at the time of compilation,
	 * e.g., /home/dknuth/ACS/motor/controller.cpp . A more globally valid file
	 * name designation could be, for instance, $ ACS_ROOT/MACI/Activator.cpp .
	 */
	private String file;

	/**
	 * The line number in the source code where the log entry was submitted.
	 */
	private int line;

	/**
	 * The fully�qualified name of the subroutine (function) where the log entry
	 * was submitted from, for example Activator::Init or just init() .
	 */
	private String routine;

	public SCInformation(Attributes atts) {
		file = atts.getValue("", "File");
		try {
			line = Integer.parseInt(atts.getValue("", "Line"));
		} catch (Exception e) {
		}
		routine = atts.getValue("", "Routine");
	}

	@Override
	public String toString() {
		return String.format(formatstr, file, line, routine);
	}
	
	public String getFile() {
		return file;
	}

	public String getRoutine() {
		// TODO Auto-generated method stub
		return routine;
	}
	
	public int getLine() {
		return line;
	}
}
