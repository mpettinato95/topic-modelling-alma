package system.event;

import java.io.Serializable;

import org.xml.sax.Attributes;

/**
 * The log element has a RuntimeContextInformation, which has six attributes
 * that give more information regarding the runtime context in which the log
 * entry is submitted.
 * 
 * @author Michele
 *
 */
public class RCInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5597422100044191844L;

	/**
	 * The name of the computer on which the log entry is generated.
	 */
	private String host;

	/**
	 * The name of the process from which the log entry is generated.
	 */
	private String process;

	/**
	 * The identification of the thread. For example, in C++ the identification
	 * is the name of the thread as supplied to InitThread
	 */
	private String thread;

	/**
	 * Any additional context information supplied by the issuer of the log
	 * entry. For example, the name of the configuration database that is being
	 * used could be put here.
	 */
	private String sourceObject;

	/**
	 * Any additional context information supplied by the issuer of the log
	 * entry. For example, the name of the configuration database that is being
	 * used could be put here.
	 */
	private String context;

	/**
	 * Identification of a bundle of related log entries. All log entries in a
	 * bundle are caused by the same �root� log entry (e.g., the original cause
	 * of an error)
	 */
	private String stackID;

	/**
	 * Specification of the number of the log entries in the bundle a given
	 * entry that have caused the log entry. The root log entry has a StackLevel
	 * of 0, the immediate log entries caused by the root log entry have a
	 * StackLevel of 1, etc.
	 */
	private int stackLevel;

	public static final String formatstr = "%-10s %-45s %-65s %-45s ";

	public RCInformation(Attributes atts) {
		host = atts.getValue("", "Host");
		process = atts.getValue("", "Process");
		thread = atts.getValue("", "Thread");
		sourceObject = atts.getValue("", "SourceObject");
		context = atts.getValue("", "Context");
		stackID = atts.getValue("", "StackId");
		try {
			stackLevel = Integer.parseInt(atts.getValue("", "StackLevel"));
		} catch (Exception e) {
		}
	}

	public String toString() {
		return String.format(formatstr, host, process, thread, sourceObject);
	}

	public String getProcess() {
		return process;
	}
	
	public String getHost() {
		return host;
	}
	
	public String getThread() {
		return thread;
	}
	
	public String getComponent() {
		return sourceObject;
	}
	
}
