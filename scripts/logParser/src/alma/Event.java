package alma;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.bind.DatatypeConverter;

import system.event.Data;

/**
 * This class models an entry in a log, i.e. an event. An event has a timestamp,
 * a message and an ordinal indicating its position on the log.
 * 
 * @author Michele
 *
 */
public class Event implements Serializable, DataSupplier {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2731805870795104570L;

	/**
	 * From
	 * 
	 * The timestamp is a mandatory attribute of every log entry. It specifies
	 * the exact time when the log entry was submitted. The time is encoded in
	 * ISO 8601 format with a precision to one millisecond. The time is
	 * specified in TAI.
	 **/
	protected final Calendar timeStamp;

	/**
	 * 
	 * 
	 * The optional log entry message is a string of characters. The message can
	 * be either an XML formatted string, or a CDATA section. The only rule it
	 * must obey is not to contain a subｭstring ]]> or characters such as �<�,
	 * �>� or �&�, since it terminates a CDATA section.
	 */
	protected String message;

	/**
	 * The position of this event in the log. This is an absolute position, i.e.
	 * this is not the position on a peculiar log file but on the overall log.
	 */
	protected int ordinal;

	/**
	 * Standard timestamp ISO format.
	 */
	protected static SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSS");

	/**
	 * Object initialization.
	 * 
	 * @param ts
	 *            an ISO format string.
	 * @param ordinal
	 *            the position of this event in the log.
	 */
	public Event(String ts, int ordinal) {
		timeStamp = DatatypeConverter.parseDateTime(ts);
		this.ordinal = ordinal;
	}

	public void setMessage(String d) {
		message = d;
	}

	public String getMessage() {
		return message;
	}

	public Calendar getTimestamp() {
		return timeStamp;
	}

	public int getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcess() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getComponent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Data> getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAudience() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getThread() {
		// TODO Auto-generated method stub
		return null;
	}

}
