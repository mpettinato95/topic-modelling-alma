package alma;

/**
 * This exception is thrown during the parse procedure of the log.
 * @author Michele
 *
 */
public class ParseException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7418756290065977887L;

	public ParseException() {
		super();
	}
	
	public ParseException(String message) {
		super(message);
	}
	
	public ParseException(Exception e) {
		super(e);
	}

}
