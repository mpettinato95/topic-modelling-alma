package alma;

import java.util.ArrayList;
import java.util.Calendar;

import system.event.Data;

public interface DataSupplier {
	
	public Calendar getTimestamp();
	
	public String getMessage();
	
	public String getType();
	
	public String getFile();
	
	public String getThread();
	
	public String getProcess();
	
	public String getComponent();
	
	public ArrayList<Data> getData();
	
	public String getHost();
	
	public String getAudience();
	
	public int getOrdinal();
	
}
