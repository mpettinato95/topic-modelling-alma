package alma;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


/**
 * This class represents a system log of ALMA. Each log objects accounts for the
 * number of log files it represents.
 * 
 * @author Michele
 *
 * @param <E> A class extending a general alma event. 
 */
public abstract class Log<E extends Event> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2822682840970737412L;

	public static final int MAX_ROWS = 1000; // As many events should be printed
	// out with toString()

	/**
	 * The list of events held by the log.
	 */
	protected final ArrayList<E> events;

	/**
	 * Account for the number of log files parsed.
	 */
	protected int fileCount = 0;

	/**
	 * Object initialization.
	 * 
	 * @throws ParseException
	 *             if this object could not be initialized. It is up to the
	 *             implementing class to override the abstract method init() and
	 *             to throw the exception. init() is just called by this
	 *             constructor.
	 */
	public Log() throws ParseException {
		events = new ArrayList<>();
		init();
	}

	/**
	 * Add the event to the events list of this log.
	 * 
	 * @param event
	 *            the event to be added.
	 */
	public void add(E event) {
		events.add(event);
	}

	public ArrayList<E> getEvents() {
		return events;
	}

	/**
	 * Parse a log file and add its entries to this log.
	 * 
	 * @param filename
	 *            the file to be parsed.
	 * @throws ParseException
	 *             If an error occurred while parsing the file.
	 * @throws IOException
	 *             If an I/O exception occurred.
	 */
	public void parse(String filename) throws ParseException, IOException {
		System.out
				.println((fileCount + 1) + ". Parsing of " + filename + "...");
		try {
			long startTime = 0, estimatedTime = 0;
			startTime = System.nanoTime();
			parseProcedure(filename);
			estimatedTime = System.nanoTime() - startTime;
			estimatedTime /= 1000000;
			fileCount++;
			System.out.println("Parsing completed. Elapsed time: "
					+ estimatedTime + " milliseconds.");
		} catch (ParseException e) {
			e.printStackTrace();
			System.err.println("Parsing failed.");
			throw e;
		}
	}

	/**
	 * Build a list of events from a given one, such that their timestamp lies
	 * on a specific time interval.
	 * 
	 * @param evs
	 *            the starting list of events.
	 * @param lower
	 *            the lower bound of the timestamp.
	 * @param upper
	 *            the upper bound of the timestamp.
	 * @return a list of events whose timestamp lies on the specified time
	 *         interval.
	 * @throws java.text.ParseException
	 *             This is not to be confused with an alma log ParseException. It
	 *             should never be thrown, it is left just in case.
	 */
	public static <E extends DataSupplier> ArrayList<E> getByTimestamp(
			ArrayList<E> evs, String lower, String upper)
			throws java.text.ParseException {
		return getByTimestamp(evs, "yyyy-MM-dd'T'HH:mm:ss.SSS", lower, upper);
	}

	/**
	 * Build a list of events from a given one, such that their timestamp lies
	 * on a specific time interval. The output is in the specified format. See
	 * SimpleDateFormat for reference.
	 * 
	 * @param evs
	 *            the starting list of events.
	 * @param lower
	 *            the lower bound of the timestamp.
	 * @param upper
	 *            the upper bound of the timestamp.
	 * @return a list of events whose timestamp lies on the specified time
	 *         interval.
	 * @throws java.text.ParseException
	 *             This is not to be confused with a log ParseException. It is
	 *             thrown by SimpleDateFormat, see that for reference.
	 */
	public static <E extends DataSupplier> ArrayList<E> getByTimestamp(
			ArrayList<E> evs, String format, String lower, String upper)
			throws java.text.ParseException {
		DateFormat df = new SimpleDateFormat(format);
		ArrayList<E> result = new ArrayList<>();
		Calendar lowerBound = Calendar.getInstance();
		Calendar upperBound = Calendar.getInstance();
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		lowerBound.setTime(df.parse(lower));
		upperBound.setTime(df.parse(upper));
		for (E event : evs) {
			if (event.getTimestamp().after(lowerBound)
					&& event.getTimestamp().before(upperBound)) {
				result.add(event);
			}
		}
		return result;
	}

	public int getFileCount() {
		return fileCount;
	}

	/**
	 * This method is the actual parse procedure and is user-defined.
	 * 
	 * @param filename
	 *            The file to be parsed.
	 * @throws ParseException
	 *             if the parse could not be carried out.
	 * @throws IOException
	 *             if an I/O exception occurred.
	 */
	protected abstract void parseProcedure(String filename)
			throws ParseException, IOException;

	/**
	 * Restore the transient fields after de-serialization. This method is also
	 * invoked by the constructor so there should really not be any need to
	 * override that. In the future it will be reworked to a static method.
	 *
	 * @throws ParseException
	 */
	public abstract void init() throws ParseException;

}
