package rparseutility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.function.Function;

import system.event.Data;
import system.event.SystemEvent;
import alma.DataSupplier;
import alma.Event;

/**
 * Return a projection (or a select clause in sql jargon) of an attribute of the
 * events. The methods of this class are invoked through the 'select' object in
 * RParseUtility. By contract, they should also return an array of Strings and
 * can take zero to as many arguments. The names of the methods should also be
 * singular.
 * 
 * @author Michele
 *
 */
public class Projector {

	/**
	 * The memory containing the list of events of the logs.
	 */
	private Memory mem;

	/**
	 * Object initialization.
	 * 
	 * @param memory
	 *            the memory object containing the list of events of the logs.
	 */
	public Projector(Memory memory) {
		this.mem = memory;
	}

	/**
	 * Retrieve the timestamps of the events.
	 * 
	 * @return A vector of the timestamps of all the events.
	 */
	public String[] timestamp() {
		return timestamp("yyyy-MM-dd'T'HH:mm:ss.SSS");
	}

	/**
	 * Retrieve the timestamps of the events and format them.
	 * 
	 * @param format
	 *            How to format the timestamps. See SimpleDateFormat for more
	 *            information.
	 * @return A vector of the timestamps of all the events.
	 */
	public String[] timestamp(String format) {
		DateFormat df = new SimpleDateFormat(format);
		//df.setTimeZone(TimeZone.getTimeZone("UTC"));
		ArrayList<? extends DataSupplier> events = mem
				.currentEvents("timestamps");
		String[] result = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			Calendar ts = events.get(i).getTimestamp();
			result[i] = df.format(ts.getTime());
		}
		return result;
	}
	
	public String[] timestamp(int index) {
		DateFormat df = new SimpleDateFormat("yy-MM-dd'T'HH:mm:ss.SSS");
		//df.setTimeZone(TimeZone.getTimeZone("UTC"));
		return getFromDoc(index, event -> df.format(event.getTimestamp().getTime()));
	}

	/**
	 * Retrieve the messages of the events.
	 * 
	 * @return A vector of the messages of all the events
	 */
	public String[] message() {
		ArrayList<? extends DataSupplier> events = mem
				.currentEvents("messages");
		String[] result = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			result[i] = events.get(i).getMessage();
		}
		return result;
	}

	/**
	 * Retrieve the components of the events.
	 * 
	 * @return A vector of the components of all the events
	 */
	public String[] component() {
		String[] result;
		if (mem.isSystem()) {
			result = new String[mem.getSystemEvents().size()];
			for (int i = 0; i < mem.getSystemEvents().size(); i++) {
				result[i] = mem.getSystemEvents().get(i).getComponent();
			}
		} else {
			result = new String[mem.getDocuments().size()];
			for (int i = 0; i < mem.getDocuments().size(); i++) {
				result[i] = mem.getDocuments().get(i).getComponent();
			}
		}
		return result;
	}

	/**
	 * Retrieve the data of the events. All members of the list of data in a
	 * specific event are concatenated according to the toString method of
	 * system.event.Data (e.g.: "Name1 <...>; Name2 <...>; ...").
	 * 
	 * @return A vector of the data of all the events
	 */
	public String[] data() {
		String[] result = new String[mem.currentEvents("data").size()];
		for (int i = 0; i < mem.currentEvents("data").size(); i++) {
			ArrayList<Data> data = mem.currentEvents("data").get(i).getData();
			String eventData = "";
			for (Data d : data) {
				eventData += d.toString() + "; ";
			}
			result[i] = eventData.trim();
		}
		return result;
	}

	/**
	 * Retrieve the containers of the events.
	 * 
	 * @return An array containing the names of the containers.
	 */
	public String[] container() {
		ArrayList<? extends DataSupplier> events = mem
				.currentEvents("containers");
		String[] result = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			result[i] = events.get(i).getProcess();
		}
		return result;
	}

	/**
	 * Retrieve the hosts of the events.
	 * 
	 * @return An array containing the hosts.
	 */
	public String[] host() {
		ArrayList<? extends DataSupplier> events = mem.currentEvents("hosts");
		String[] result = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			result[i] = events.get(i).getHost();
		}
		return result;
	}

	/**
	 * Retrieve the log entry types of the events.
	 * 
	 * @return An array containing the log entry types of the events.
	 */
	public String[] type() {
		ArrayList<? extends DataSupplier> events = mem.currentEvents("types");
		String[] result = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			result[i] = events.get(i).getType().toString();
		}
		return result;
	}

	/**
	 * Retrieve the source codes of the events.
	 * 
	 * @return A vector containing the source codes of the events.
	 */
	public String[] file() {
		ArrayList<? extends DataSupplier> events = mem.currentEvents("files");
		String[] result = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			result[i] = events.get(i).getFile();
		}
		return result;
	}

	/**
	 * Retrieve the audience of the events.
	 * 
	 * @return A vector containing the audience of the events.
	 */
	public String[] audience() {
		ArrayList<? extends DataSupplier> events = mem
				.currentEvents("audiences");
		String[] result = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			result[i] = events.get(i).getAudience();
		}
		return result;
	}

	/**
	 * Retrieve what files (in absolute path) threw an exception. To find out
	 * the exception that was thrown, please see Selector.failure.
	 * 
	 * @return A vector of files that threw an exception.
	 */
	public String[] failure() {
		String[] result = new String[mem.failures.size()];
		int i = 0;
		for (String failure : mem.failures.keySet()) {
			result[i++] = failure;
		}
		return result;
	}

	public String[] error() {
//		String[] result = new String[mem.getContainerEvents().size()];
//		for (int i = 0; i < mem.getContainerEvents().size(); i++) {
//			result[i] = Integer.toString(mem.getContainerEvents().get(i)
//					.getErrorCount());
//		}
//		return result;
		return null;
	}
	
	public String[] type(int index) {
		return getFromDoc(index, e -> e.getType());
	}

	public String[] document(int index) {
		return getFromDoc(index, e -> e.getMessage());
	}
	
	public String[] file(int index) {
		return getFromDoc(index, e -> e.getFile() 
				+ ", " + e.getRoutine() + ", " + e.getLine());
	}

	private String[] getFromDoc(int index, Function<SystemEvent, String> func) {
		index--;
		SystemEvent[] events = mem.getDocuments().get(index).getEvents();
		String[] result = new String[events.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = func.apply(events[i]);
		}
		return result;
	}
	
	public String[] thread() {
		ArrayList<? extends DataSupplier> events = mem.currentEvents("threads");
		String[] result = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			result[i] = events.get(i).getThread().toString();
		}
		return result;
	}

	public String[] thread(int index) {
		return getFromDoc(index, e -> e.getThread());
	}

	// public String[] docError() {
	// ArrayList<Document> docs = Document.getDocuments();
	// String[] result = new String[docs.size()];
	// for (int i = 0; i < docs.size(); i++) {
	// result[i] = Integer.toString(docs.get(i).getNumberOfErrors());
	// }
	// return result;
	// }
	//
	// public String[] docComponent() {
	// ArrayList<Document> docs = Document.getDocuments();
	// String[] result = new String[docs.size()];
	// for (int i = 0; i < docs.size(); i++) {
	// result[i] = docs.get(i).getComponent();
	// }
	// return result;
	// }
	//
	// public String[] docTimeSpan() {
	// ArrayList<Document> docs = Document.getDocuments();
	// String[] result = new String[docs.size()];
	// for (int i = 0; i < docs.size(); i++) {
	// result[i] = docs.get(i).getTimeSpan();
	// }
	// return result;
	// }

	/**
	 * Retrieve the ordinal of the events, i.e. in what order they appear in the
	 * log.
	 * 
	 * @return A vector containing the ordinal of the events.
	 */
	public String[] ordinal() {
		ArrayList<? extends DataSupplier> events = mem
				.currentEvents("ordinals");
		String[] result = new String[events.size()];
		int i = 0;
		for (DataSupplier event : events) {
			result[i++] = Integer.toString(event.getOrdinal());
		}
		return result;
	}
}
