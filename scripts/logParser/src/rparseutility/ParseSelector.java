package rparseutility;

import java.util.Arrays;

import system.log.LogEntryType;

/**
 * This class performs the selections BEFORE commencing the parse. That is what
 * differ from the Selector. The ParseSelector is useful to improve the
 * performance of the parse, when it is known in advance that some data is not
 * needed. By contract, the return type of its methods are void and they can
 * have as many arguments as needed. The methods are invoked by the 'search'
 * object in RParseUtility. Their names should also begin with 'for' followed by
 * what one is searching for.
 * 
 * There are generally three versions of the same method: one with a string argument,
 * another one with a string and a boolean (enable/disable selection) and finally a 'forAll' version.
 * 
 * @author Michele
 *
 */
public class ParseSelector {

	/**
	 * The object, containing the logs, that also provides tools for managing
	 * them.
	 */
	private Memory mem;

	/**
	 * Object initialization.
	 * 
	 * @param memory
	 */
	public ParseSelector(Memory memory) {
		this.mem = memory;
	}

	/**
	 * Parse only for the specified log entry type.
	 * 
	 * @param type
	 *            the log entry type to be parsed, case insensitive.
	 */
	public void forType(String type) {
		forAllTypes(false);
		forType(type, true);
	}

	/**
	 * Enable or disable the parse of a specific log entry type.
	 * 
	 * @param type
	 *            the log entry type to be parsed, case insensitive.
	 * @param newFlag
	 *            whether the log entry type should be parsed.
	 */
	public void forType(String type, boolean newFlag) {
		LogEntryType result = null;
		for (LogEntryType let : LogEntryType.values()) {
			if (let.toString().equals(type.toUpperCase())) {
				result = let;
			}
		}
		if (result != null) {
			result.setParseFlag(newFlag);
		} else {
			System.err.println("Type not found.");
		}
	}

	/**
	 * Enable or disable the parse of ALL log entry types.
	 * 
	 * @param newFlag
	 *            whether to parse all log entry types.
	 */
	public void forAllTypes(boolean newFlag) {
		LogEntryType.setAllParseFlags(newFlag);
	}

	/**
	 * Parse ALL log entry types.
	 */
	public void forAllTypes() {
		LogEntryType.setAllParseFlags(true);
	}

	/**
	 * Parse only for the specified container.
	 * 
	 * @param container
	 */
	public void forContainer(String container) {
		mem.getSystemLog().getContainers().clear();
		mem.getSystemLog().getContainers().add(container);
	}

	/*
	 * WARNING: This method should be tested again!
	 */
	/**
	 * Enable or disable the parse of a specific container.
	 * 
	 * @param container
	 * @param flag
	 */
	public void forContainer(String container, boolean flag) {
		boolean wasThere;
		if (flag) {
			wasThere = ! mem.getSystemLog().getContainers().add(container);
			if (wasThere) {
				System.out.println("This container is already enabled!");
			}
		} else {
			wasThere = mem.getSystemLog().getContainers().remove(container);
			if (!wasThere) {
				System.out.println("This container is already not enabled!");
			}
		}
	}

	/**
	 * Enable the parse of all containers.
	 */
	public void forAllContainers() {
		mem.getSystemLog().getContainers().clear();
	}

	/**
	 * Enable the parse for all the chosen containers.
	 * @param containers what containers should be searched for.
	 */
	public void forContainers(String[] containers) {
		mem.getSystemLog().getContainers().clear();
		mem.getSystemLog().getContainers().addAll(Arrays.asList(containers));
	}

	/**
	 * Remove all applied selections. 
	 */
	public void reset() {
		forAllTypes();
		forAllContainers();
	}
}
