package rparseutility;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import alma.DataSupplier;
import alma.Event;
import alma.Log;
import alma.ParseException;
import system.event.SystemEvent;
import system.log.SystemLog;

/**
 * This class contains the logs with their lists of events. It provides methods
 * to save, load, clean up and others to help the classes in rparseutility
 * managing the logs.
 * 
 * @author Michele
 *
 */
public class Memory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6511411697108101L;

	/**
	 * The log object containing the events in the SYSTEM folder.
	 */
	private SystemLog systemLog;
	
	private ArrayList<Document> docs;

	private ArrayList<Document> backupDocs;
	
	/**
	 * Set of log files that were meant to be parsed, but produced a failure in
	 * the process. This set can be consulted to fix these logs and repeat the
	 * procedure with RParseUtility.parseFailures(). The files are stored with
	 * their absolute path.
	 */
	HashMap<String, Exception> failures;


	/**
	 * The list of system events that is modified when applying a selection.
	 */
	private transient ArrayList<SystemEvent> systemEvents;
	

	/**
	 * The list of system events that is set when calling reset(). This value
	 * set by the user through mark(). The only exception is when the log is
	 * (re)initialized, namely via the methods loadstate(), clear() and the
	 * constructor.
	 */
	private transient ArrayList<SystemEvent> systemMark;

	/**
	 * Whether the ambiguously-named operation (e.g.: select("timestamp"))
	 * should operate on the system events or on the container's. This value is
	 * set by from().
	 */
	private transient boolean fromSystem;

	/**
	 * Whether the program should print a message (e.g.:
	 * "timestamps from SYSTEM") when an ambiguously-named operation is
	 * executed. This value is set by from().
	 */
	private transient boolean feedbackOn;

	/**
	 * Object initialization.
	 * 
	 * @throws ParseException
	 *             If the initialization of the logs was not successful.
	 */
	public Memory() throws ParseException {
		systemLog = new SystemLog();
		systemEvents = systemLog.getEvents();
		docs = new ArrayList<>();
		systemMark = systemEvents;
		failures = new HashMap<>();
		fromSystem = true;
		feedbackOn = true;
	}

	/**
	 * Write all the non-transient fields to the specified path.
	 * 
	 * @param path
	 *            Where to output the logs.
	 */
	public void saveState(String path) {
		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(path))) {
			long startTime = 0, estimatedTime = 0;
			startTime = System.nanoTime();
			oos.writeObject(this);
			estimatedTime = System.nanoTime() - startTime;
			estimatedTime /= 1000000;
			println("Estimated time: " + estimatedTime + " ms");
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
//	
//	public void saveDocument(String path) {
//		try (ObjectOutputStream oos = new ObjectOutputStream(
//				new FileOutputStream(path))) {
//			long startTime = 0, estimatedTime = 0;
//			startTime = System.nanoTime();
//			oos.writeObject(docs);
//			estimatedTime = System.nanoTime() - startTime;
//			estimatedTime /= 1000000;
//			println("Estimated time: " + estimatedTime + " ms");
//		} catch (IOException i) {
//			i.printStackTrace();
//		}
//	}
//	
//	public static void loadDocuent(String path) {
//	try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
//			path))) {
//		long startTime = 0, estimatedTime = 0;
//		System.out.println("Initial number of documents: " + Document.getDocuments().size());
//		startTime = System.nanoTime();
//		Document.getDocuments().addAll((ArrayList<Document>) ois.readObject());
//		estimatedTime = System.nanoTime() - startTime;
//		estimatedTime /= 1000000;
//		System.out.println("Estimated time: " + estimatedTime + " ms");
//	} catch (IOException | ClassNotFoundException e) {
//		e.printStackTrace();
//	}
//}

	/**
	 * Load the log objects from a file.
	 * 
	 * @param path
	 *            The file to be read.
	 * @throws Exception
	 */
	public void loadState(String path) throws Exception {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
				path))) {
			long startTime = 0, estimatedTime = 0;
			startTime = System.nanoTime();
			Memory memory = (Memory) ois.readObject();
			systemLog = memory.systemLog;
			systemEvents = systemLog.getEvents();
			systemMark = systemEvents;
			docs = memory.docs;
			failures = memory.failures;
			systemLog.init();
			estimatedTime = System.nanoTime() - startTime;
			estimatedTime /= 1000000;
			println("Estimated time: " + estimatedTime + " ms");
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Clean up the memory, but only the part specified with from(). This method
	 * also removes some search selection. Note that this method builds a fresh
	 * new instance of the log.
	 * 
	 * @throws ParseException
	 */
	public void clear() throws ParseException {
		if (fromSystem) {
			systemLog = new SystemLog();
			systemEvents = systemLog.getEvents();
			systemMark = systemEvents;
			println("clear from SYSTEM\n");
		} else {
//			containerLog = new ContainerLog();
//			containerEvents = containerLog.getEvents();
//			containerMark = containerEvents;
//			println("clear from CONTAINER\n");
			println("clear from DOCUMENT\n");
			docs.clear();
		}
	}

	/**
	 * Permanently apply all current selections. This process cannot be
	 * reverted.
	 */
	public void apply() {
		systemLog.setEvents(systemEvents);
		for (int i = 0; i < systemEvents.size(); i++) {
			systemEvents.get(i).setOrdinal(i + 1);
		}
	}

	public void mark() {
		if (fromSystem) {
			systemMark = systemEvents;
		} else {

		}
	}

	/**
	 * Specify what log the ambiguously-named fields and operation refer to. It
	 * is also possible to enable/disable the debug message.
	 * 
	 * @param log
	 *            Works only if this argument begins with an s (for system) or a
	 *            c (for container), case insensitive. A message is printed in
	 *            case of incorrect input.
	 * @param feedbackOn
	 *            whether the debug message concerning most of the project and
	 *            select operations should be on or off.
	 */
	public void from(String log, boolean feedbackOn) {
		log = log.toLowerCase().trim();
		this.feedbackOn = feedbackOn;
		if (log.startsWith("s")) {
			fromSystem = true;
		} else if (log.startsWith("d")) {
			fromSystem = false;
		} else {
			System.out.println("command not acknowledged.");
		}
	}

	public boolean isSystem() {
		return fromSystem;
	}

	/**
	 * Return a list of events according to the current setting of from().
	 * 
	 * @param methodsName
	 *            the name of the method wanting to get the unknown list of
	 *            events. This is needed just for the debug message.
	 * @return a list of unknown events.
	 */
	ArrayList<? extends DataSupplier> currentEvents(String methodsName) {
		if (fromSystem) {
			println("\n" + methodsName + " from SYSTEM");
			return systemEvents;
		} else {
			println("\n" + methodsName + " from DOCUMENT");
			return docs;
		}
	}

	/**
	 * Return a list of events according to the current setting of from().
	 * 
	 * @param fieldsName
	 *            the name of the field to be retrieved from the log. This is
	 *            needed just for the debug message.
	 * @return a list of unknown events.
	 */
	Log<? extends Event> currentLog(String fieldsName) {
		if (fromSystem) {
			println("\n" + fieldsName + " from SYSTEM");
			return systemLog;
		} else {
			return null;
		}
	}

	public ArrayList<SystemEvent> getSystemEvents() {
		return systemEvents;
	}

	public SystemLog getSystemLog() {
		return systemLog;
	}

	public void setSystemLog(SystemLog systemLog) {
		this.systemLog = systemLog;
	}

	/**
	 * Setter for the list of SystemEvents that prints also a debug message.
	 * 
	 * @param containerEvents
	 */
	public void setSystemEvents(ArrayList<SystemEvent> systemEvents) {
		this.systemEvents = systemEvents;
		println();
		println("count from SYSTEM\n" + systemEvents.size());
	}

	/**
	 * Setter for the list of Events that operates according to the current
	 * setting of from(). This also prints a debug message.
	 * 
	 * @param events
	 *            a list of unknown events.
	 */
	public void setCurrentEvents(ArrayList<? extends DataSupplier> events) {
		if (fromSystem) {
			systemEvents = (ArrayList<SystemEvent>) events;
			println();
			println("count from SYSTEM\n" + systemEvents.size());
		} else {

		}
	}

	/**
	 * Backtrack to the previous mark.
	 */
	public void reset() {
		if (fromSystem) {
			systemEvents = systemMark;
			println();
			println("count from SYSTEM\n" + systemEvents.size());
		} else {
		}
	}

	/**
	 * Backtrack to the original list of events.
	 */
	public void rewind() {
		if (fromSystem) {
			systemEvents = systemLog.getEvents();
			println();
			println("count from SYSTEM\n" + systemEvents.size());
		} else {
//			containerEvents = containerLog.getEvents();
//			println();
//			println("count from CONTAINER\n" + containerEvents.size());
			docs = (ArrayList<Document>) backupDocs.clone();
		}
	}

	/**
	 * Print a new line only if the current setting of from() allows that.
	 */
	private void println() {
		if (feedbackOn) {
			System.out.println();
		}
	}

	/**
	 * Print a message only if the current setting of from() allows that.
	 * 
	 * @param message
	 *            what to be printed.
	 */
	private void println(String message) {
		if (feedbackOn) {
			System.out.println(message);
		}
	}

	public ArrayList<Document> getDocuments() {
		return docs;
	}
	
	public ArrayList<Document> getBackupDocuments() {
		return backupDocs;
	}
	
}
