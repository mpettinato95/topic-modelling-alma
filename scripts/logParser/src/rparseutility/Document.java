package rparseutility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import alma.DataSupplier;
import alma.Event;
import system.event.Data;
import system.event.SystemEvent;

public class Document implements Serializable, DataSupplier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -327797103111L;

	private SystemEvent[] doc;

	public Document(List<? extends Event> doc) {
		this.doc = doc.toArray(new SystemEvent[doc.size()]);
	}
	
	

	public static void appendComponent(Memory mem) throws FileNotFoundException {
		final String BEGIN = "Beginning SB execution.";
		int from;
		ArrayList<SystemEvent> events = sortDocs(mem);
		for (from = 0; from < events.size()
				&& !events.get(from).getMessage().equals(BEGIN); from++)
			;
		if (from >= events.size())
			return;
		for (int to = from + 1; to < events.size(); to++) {
			if (events.get(to).getMessage().equals(BEGIN)) {
				appendDoc(mem, events, from, to);
				from = to;
				
			}
		}
		appendDoc(mem, events, from, events.size());
		
	}



	private static void appendDoc(Memory mem, ArrayList<SystemEvent> events, int from,
			int to) {
		List<SystemEvent> sequence = events.subList(from, to);
		mem.getDocuments().add(new Document(sequence));
		System.out.println("Document Appended");
	}



	private static ArrayList<SystemEvent> sortDocs(Memory mem) throws FileNotFoundException {
		ArrayList<SystemEvent> events = mem.getSystemEvents();
		events.sort((e1, e2) -> e1.getTimestamp().compareTo(e2.getTimestamp()));
//		try (PrintWriter pw = new PrintWriter(new FileOutputStream(new File("debug_sortedDoc.txt"), true))) {
//			for (SystemEvent e : events) {
//				pw.append(new SimpleDateFormat(
//						"yyyy-MM-dd'T'HH:mm:ss.SSS").format(e.getTimestamp()
//						.getTime())).append("\n");
//			}
//		}
		return events;
	}

	public static void append(Memory mem) {
		ArrayList<SystemEvent> events = mem.getSystemEvents();
		HashMap<String, ArrayList<Event>> map = new HashMap<>();
		for (SystemEvent event : events) {
			ArrayList<Event> doc;
			String component = event.getComponent();
			if (!map.containsKey(component)) {
				doc = new ArrayList<>();
				map.put(component, doc);
			} else {
				doc = map.get(component);
			}
			doc.add(event);
		}
		// to a more efficient data structure.
		for (ArrayList<Event> doc : map.values()) {
			mem.getBackupDocuments().add(new Document(doc));
			mem.from("d", true);
			mem.rewind();
		}
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (Event e : doc) {
			builder.append(e.getMessage().replace("\n", " "));
			builder.append("\n");
		}
		return builder.toString();
	}

	@Override
	public Calendar getTimestamp() {
		return doc[0].getTimestamp();
	}

	@Override
	public String getMessage() {
		return toString();
	}

	@Override
	public String getType() {
		return listGen(e -> e.getType());
	}

	@Override
	public String getFile() {
		return listGen(e -> e.getFile());
	}

	@Override
	public String getProcess() {
		return listGen(e -> e.getProcess());
	}

	@Override
	public String getComponent() {
		return doc[0].getComponent();
	}

	private String listGen(Function<Event, String> func) {
		StringBuilder b = new StringBuilder("[");
		for (Event e : doc) {
			b.append(func.apply(e));
			b.append(",\n");
		}
		b.append("]");
		return b.toString();
	}

	@Override
	public ArrayList<Data> getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAudience() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getOrdinal() {
		// TODO Auto-generated method stub
		return -1;
	}

	public SystemEvent[] getEvents() {
		return doc;
	}

	@Override
	public String getThread() {
		return listGen(e -> e.getThread());
	}

	// @SuppressWarnings("unchecked")
	// public static void loadState(String path) {
	// try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
	// path))) {
	// long startTime = 0, estimatedTime = 0;
	// System.out.println("Initial number of documents: " + docs.size());
	// startTime = System.nanoTime();
	// docs.addAll((ArrayList<Document>) ois.readObject());
	// estimatedTime = System.nanoTime() - startTime;
	// estimatedTime /= 1000000;
	// System.out.println("Estimated time: " + estimatedTime + " ms");
	// } catch (IOException | ClassNotFoundException e) {
	// e.printStackTrace();
	// }
	// }

}
