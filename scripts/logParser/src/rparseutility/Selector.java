package rparseutility;

import java.text.ParseException;
import java.util.ArrayList;
import alma.DataSupplier;
import alma.Event;
import alma.Log;
import system.event.SystemEvent;
import system.log.SystemLog;

/**
 * Apply a selection (or a where clause in sql jargon) to one of the logs stored
 * in Memory. The methods of this class are invoked through the 'where' object
 * in RParseUtility. By contract the methods of this class must have void return
 * value and an unbound number of arguments. Their name should also be singular.
 * 
 * @author Michele
 *
 */
public class Selector {

	/**
	 * The memory containing the logs.
	 */
	private Memory mem;

	/**
	 * Object initialization
	 * 
	 * @param memory
	 *            the memory containing the logs.
	 */
	public Selector(Memory memory) {
		this.mem = memory;
	}

	/**
	 * Apply a selection according to the specified containers.
	 * 
	 * @param containers
	 *            the containers to be selected.
	 */
	public void container(String... containers) {
		if (mem.isSystem()) {
			mem.setSystemEvents(SystemLog.getByContainer(mem.getSystemEvents(),
					containers));
		} else {
//			mem.setContainerEvents(ContainerLog.getByContainer(
//					mem.getContainerEvents(), containers));
		}
	}

	/**
	 * Apply a selection according to the specified containers.
	 * 
	 * @param containers
	 *            the containers to be selected.
	 */
	public void containerWithRemove(String... containers) {
		mem.setSystemEvents(mem.getSystemLog().getByContainerAndRemove(
				mem.getSystemEvents(), containers));
	}

	public void component(String... components) {
		if (mem.isSystem()) {
			mem.setSystemEvents(SystemLog.getByComponent(mem.getSystemEvents(),
					components));
		} else {
			for (Document doc : mem.getDocuments()) {
				for (String comp : components) {
					if (comp.equals(doc.getComponent())) {
						
					}
				}
			}
		}
	}

	/**
	 * Apply a selection according to the specified audiences.
	 * 
	 * @param audiences
	 *            the audiences to be selected.
	 */
	public void audience(String... audiences) {
		mem.setSystemEvents(SystemLog.getByAudience(mem.getSystemEvents(),
				audiences));
	}

	/**
	 * Apply a selection according to the specified log entry types.
	 * 
	 * @param types
	 *            the log entry types to be selected.
	 */
	public void type(String... types) {
		mem.setSystemEvents(SystemLog.getByType(mem.getSystemEvents(), types));
	}
	
	public void thread(String... threads) {
		mem.setSystemEvents(SystemLog.getByThread(mem.getSystemEvents(), threads));
	}

	/**
	 * Apply a selection according to the specified files.
	 * 
	 * @param files
	 *            the files to be selected.
	 */
	public void file(String... files) {
		mem.setSystemEvents(SystemLog.getByFile(mem.getSystemEvents(), files));
	}

	/**
	 * Apply a selection according to the chosen timestamps interval.
	 * 
	 * @param args
	 *            this method accepts only 3 or 2 arguments. The former version
	 *            includes the format of the timestamp as first parameter,
	 *            followed then by the lower bound and upper bound of the time
	 *            interval. The latter does not require the format but only the
	 *            bounds.
	 * @throws ParseException
	 */
	public void timestamp(String... args) throws ParseException {
		ArrayList<? extends DataSupplier> events = mem
				.currentEvents("timestamps");
		if (args.length > 2) {
			mem.setCurrentEvents(Log.getByTimestamp(events, args[0], args[1],
					args[2]));
		} else {
			mem.setCurrentEvents(Log.getByTimestamp(events, args[0], args[1]));
		}
	}

	public void ordinal(String... patterns) {
		ArrayList<SystemEvent> result = new ArrayList<SystemEvent>();
		mem.rewind();
//		System.out.println("<Debug> ordinal1");
		for (String pattern : patterns) {
//			System.out.println("<Debug> ordinalL1");
			String[] interval = pattern.split(":");
//			System.out.println("<Debug> ordinalL2");
			if (interval.length == 2) {
//				System.out.println("<Debug> ordinalL3");
				int lowerBound = Integer.parseInt(interval[0]);
//				System.out.println("<Debug> ordinalL4");
				int upperBound = Integer.parseInt(interval[1]);
//				System.out.println("<Debug> ordinalL5");
				result.addAll(SystemLog.getByOrdinal(mem.getSystemEvents(),
						lowerBound, upperBound));
//				System.out.println("<Debug> ordinalL6");
			}
		}
//		System.out.println("<Debug> ordinal2");
		mem.setSystemEvents(result);
//		System.out.println("<Debug> ordinal3");
	}

	/**
	 * Apply a selection according to the specified patterns in the messages.
	 * The patterns are evaluated as regex.
	 * 
	 * @param messages
	 *            the patterns the messages should comply with.
	 */
	public void message(String... messages) {
		mem.setSystemEvents(SystemLog.getByMessage(mem.getSystemEvents(),
				messages));
	}

	/**
	 * Apply a selection by choosing messages that do NOT comply with the
	 * specified patterns.
	 * 
	 * @param messages
	 *            the patterns the messages should NOT comply with.
	 */
	public void messageNot(String... messages) {
		ArrayList<SystemEvent> remove = SystemLog.getByMessage(
				mem.getSystemEvents(), messages);
		setNegativeResult(remove);
	}

	/*
	 * Procedure to remove events from the list of events.
	 */
	private void setNegativeResult(ArrayList<SystemEvent> remove) {
		ArrayList<SystemEvent> result = (ArrayList<SystemEvent>) mem
				.getSystemEvents().clone(); // if no selection is on, the
											// list of the events is backed by
											// the log itself, hence the list
											// has to be cloned to avoid
											// undesired effects.
		result.removeAll(remove);
		mem.setSystemEvents(result);
	}

	/**
	 * Apply a selection according to the specified patterns in the data.
	 * 
	 * @param data
	 *            The patterns the data should compy with.
	 */
	public void data(String... data) {
		mem.setSystemEvents(SystemLog.getByData(mem.getSystemEvents(), data));
	}

	/**
	 * Apply a selection by choosing data that do NOT comply with the specified
	 * patterns.
	 * 
	 * @param data
	 *            the patterns the data should NOT comply with.
	 */
	public void dataNot(String... data) {
		ArrayList<SystemEvent> remove = SystemLog.getByData(
				mem.getSystemEvents(), data);
		setNegativeResult(remove);
	}

	/**
	 * Print specific ParseExceptions according to the absolute file name. To
	 * find out what files threw an exception, please see Projector.failure.
	 * 
	 * @param absoluteFileNames
	 *            the absolute file names whose exception should be printed.
	 */
	public void failure(String... absoluteFileNames) {
		for (int i = 0; i < absoluteFileNames.length; i++) {
			mem.failures.get(absoluteFileNames[i]).printStackTrace();
		}
	}
}
