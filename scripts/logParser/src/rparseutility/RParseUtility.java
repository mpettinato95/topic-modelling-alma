package rparseutility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import alma.ParseException;

/**
 * Provide means of communication between R and Java. This class converts the
 * results of java methods in vectors or primitive values, easily comprehensible
 * by R.
 * 
 * @author Michele
 *
 */
public class RParseUtility {

	private Projector select;
	private Selector where;
	private ParseSelector search;
	private Memory mem;

	private int parseCount;

	private int failCount;

	private boolean autoSkip;

	private long startTime, elapsedTime;

	/**
	 * Provides utilities for R.
	 * 
	 * @param systemLog
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public RParseUtility() throws ParseException {
		mem = new Memory();
		select = new Projector(mem);
		where = new Selector(mem);
		search = new ParseSelector(mem);
		startTime = 0;
	}

	public void initParse(boolean autoSkip) {
		elapsedTime = 0;
		parseCount = 0;
		failCount = 0;
		this.autoSkip = autoSkip;
		startTime = System.nanoTime();
	}

	public void parse(String path) throws ParseException, IOException {
		parse(path, false);
	}

	public void parse(String path, String[] errorList) throws ParseException, IOException {
//		mem.getContainerLog().setErrorList(errorList);
		parse(path, false);
	}

	public void parse(String path, String[] errorList, boolean autoSkip)
			throws ParseException, IOException {
//		mem.getContainerLog().setErrorList(errorList);
		parse(path, autoSkip);
	}

	public void parse(String path, boolean autoSkip) throws ParseException,
			IOException {
		File file = new File(path);
		if (!file.exists())
			throw new FileNotFoundException(path + " does not exists.");
		if (file.isDirectory()) {
			try {
				initParse(autoSkip);
				parseDirectory(file);
			} catch (ParseException e) {
				// terminate gracefully
			}
			elapsedTime += System.nanoTime() - startTime;
			printMetrics();
		} else {
			parseFile(file);
		}
		mem.setSystemEvents(mem.getSystemLog().getEvents());
//		mem.setContainerEvents(mem.getContainerLog().getEvents());
	}

	private void printMetrics() {
		DecimalFormat df = new DecimalFormat("0.##");
		double seconds = (double) elapsedTime / 1000000000.0;
		startTime = 0;
		System.out.println();
		System.out.println("File count: " + parseCount);
		System.out.println("Failure count: " + failCount);
		System.out.println("Elapsed time: " + df.format(seconds) + " seconds");
	}

	public void parseFailures() throws ParseException {
		parseFailures(false);
	}

	public void parseFailures(boolean autoSkip) throws ParseException {
		initParse(autoSkip);
		try {
			for (String failure : mem.failures.keySet()) {
				parseFileWithHandling(new File(failure));
			}
		} catch (ParseException e) {
			// terminate gracefully
		}
		printMetrics();
	}

	private void parseDirectory(File file) throws ParseException {
		System.out.println();
		System.out.println("---- BEGIN OF " + file.getAbsolutePath() + " ----");
		for (String name : file.list()) {
			File innerFile = new File(file.getAbsolutePath() + File.separator
					+ name);
			if (innerFile.isDirectory()) {
				parseDirectory(innerFile);
			} else {
				parseFileWithHandling(innerFile);
				System.out.println("Events count: "
						+ mem.getSystemLog().getEvents().size());
			}
		}
		System.out.println();
		System.out.println("---- END OF " + file.getAbsolutePath() + " ----");
	}

	private void parseFileWithHandling(File innerFile) throws ParseException {
		boolean retry;
		do {
			retry = false;
			try {
				parseFile(innerFile);
			} catch (ParseException | IOException e) {
				if (autoSkip) {
					addFailure(innerFile, e);
				} else {
					elapsedTime += System.nanoTime() - startTime;
					retry = userHandling(innerFile, e);
					startTime = System.nanoTime();
				}
			}
		} while (retry);
	}

	private void addFailure(File file, Exception exc) {
		mem.failures.put(file.getAbsolutePath(), exc);
		failCount++;
	}

	private boolean userHandling(File file, Exception exc)
			throws ParseException {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		boolean deciding = true, retry = false;
		System.out.println();
		System.out
				.println("RETRY the parsing of the file, SKIP this file, "
						+ "SKIP ALL the future files with exceptions or TERMINATE the execution?");
		do {
			String choice = scan.nextLine().trim().toLowerCase();
			if (choice.startsWith("r")) {
				deciding = false;
				retry = true;
			} else if (choice.startsWith("s")) {
				if (choice.contains("all")) {
					autoSkip = true;
				}
				addFailure(file, exc);
				deciding = false;
				retry = false;
			} else if (choice.startsWith("t")) {
				addFailure(file, exc);
				throw new ParseException();
			}
		} while (deciding);
		return retry;
	}

	private void parseFile(File file) throws ParseException, IOException {
		String name = file.getName();
		String outerExt = getFileExtension(name);
		System.out.println();
		if ("gz".equals(outerExt)) {
			String innerExt = getFileExtension(name.substring(0,
					name.length() - 3));
			if ("xml".equals(innerExt)) {
				mem.getSystemLog().parse(file.getAbsolutePath());
			} else {
//				mem.getContainerLog().parse(file.getAbsolutePath());
			}
			parseCount++;
			if (mem.failures.remove(file.getAbsolutePath()) != null) {
				System.out
						.println("*** Parsing this file does not fail anymore! ***");
			}
		}
	}

	private static String getFileExtension(String name) {
		int index = name.lastIndexOf('.');
		if (index > 0)
			return name.substring(index + 1);
		else
			return "";
	}

	public Projector getSelect() {
		return select;
	}

	public Selector getFilter() {
		return where;
	}

	public ParseSelector getSearch() {
		return search;
	}

	public Memory getMemory() {
		return mem;
	}

	public int fileCount() {
		return mem.currentLog("fileCount").getFileCount();
	}

	public int parseCount() {
		return parseCount;
	}

	public int lastFailureCount() {
		return failCount;
	}

	public int failureCount() {
		return mem.failures.size();
	}

	// re-factor later. Maybe remove
	public String[][] findTopics(String[][] topicsTerm) {
		int topTerms = topicsTerm[0].length;
		HashSet<String> terms = new HashSet<>();
		int resultSize = topicsTerm.length;
		for (int t1 = 0; t1 < topicsTerm.length; t1++) {
			if (topicsTerm[t1] != null) {
				for (int term = 0; term < topTerms; term++) {
					terms.add(topicsTerm[t1][term]);
				}
				int count = 0;
				for (int t2 = t1 + 1; t2 < topicsTerm.length; t2++) {
					for (int term = 0; term < topTerms; term++) {
						if (terms.contains(topicsTerm[t2][term])) {
							count++;
						}
					}
					if (count > topTerms / 2) {
						topicsTerm[t2] = null;
						resultSize--;
					}
				}
				terms.clear();
			}
		}
		String[][] result = new String[resultSize][topTerms];
		for (int i = 0, j = 0; i < topicsTerm.length; i++) {
			if (topicsTerm[i] != null) {
				result[j++] = topicsTerm[i];
			}
		}
		return result;
		// return new String[10];
	}

	/**
	 * Retrieve the number of events with the current filters.
	 * 
	 * @return the number of events.
	 */
	public int count() {
		return mem.currentEvents("count").size();
	}

	public static void main(String args[]) throws ParseException, IOException {
		RParseUtility rpu = new RParseUtility();
		rpu.search.forType("error");
		rpu.parse("D:\\logs\\AOS\\SYSTEM\\2016-02-22\\log2016-02-21T23_58_06.016_2016-02-22T00_18_00.024.xml.gz");
		String[][] asr = { { "hello", "world", "ascac", "oascoaspc", "royal" },
				{ "chicken", "meal", "royal", "hello", "world" } };
		// String[][] result = rpu.findTopics(asr);
		// int i = 1;
		// for (String[] s : result) {
		// System.out.println("Topic " + i++);
		// for (String ss : s) {
		// System.out.println(ss);
		// }
		// }
	}
}
