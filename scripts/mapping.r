unnameLogMap <- function(logMap)  {
	result <- c()
	for (i in seq(logMap)) {
		result[[i]] <- unname(logMap[[i]]$event)
	}
	result
}

colorSequences <- function(logMap) {
	from("DOCUMENT")
	outFile <- "coloredSequences.txt"
	write("", file=outFile)
	nDocs <- get("count")
	nTopics <- length(logMap[[1]])
	for (i in seq(nDocs)) {
		docLength <- length(select("document", as.integer(i)))
		out <- paste("Sequence", i, "-", docLength) 
		for (j in seq(nTopics)) {
			assignLen <- length(logMap[[i]][[j]])
			x <- assignLen / docLength
			x <- paste(format(round(x, 2), nsmall = 2), "-", assignLen)
			out <- paste(out, x, sep="\t")
		}
		write(out, file=outFile, append=TRUE)
	}
}

preProcessDocs <- function() {
	from("DOCUMENT")
	nDocs <- get("count")
	i = 1
	docs <- c()
	while (i <= nDocs) {
		docs <- c(docs, select("document", as.integer(i)))
		i = i + 1
	}
	preProcess(docs)
}

compareDictionaries <- function(lda, aDFM) {
	old <- names(posterior(lda)$terms[1, ])
	new <- features(aDFM)
	newTerms <- seq(new)[! new %in% old]
	cat("LDA model uncovered terms\n")
	print(new[newTerms])
	cat("Coverage Ratio:", 1 - length(newTerms) / length(new), "\n")
	newTerms
}

topicSequence <- function(lda) {
	procDocsDFM <- preProcessDocs()
	procDocs <- posterior(lda, procDocsDFM)$topics
	newTerms <- compareDictionaries(lda, procDocsDFM)	
	nDocs <- get("count")
	dictLog <- "newTerms.txt"
	res <- list()
	write("", file=dictLog)
	cat("Check", dictLog, "for uncovered terms\n")
	i = 1
	base = 0
	while (i <= nDocs) {
		cat("Processing sequence", i, "\n")
		i <- as.integer(i)
		doc <- select("document", i)
		topList <- c()
		for (j in seq(doc)) {
			eventDist <- procDocs[base + j, ]
			topic <- which.max(eventDist)
			# Mark events with new vocaulary
			if (sum(procDocsDFM[base + j, newTerms]) > 0) {
				data <- paste("Sequence", i, "message num", j)
				write(data, append=TRUE, file=dictLog)
			}
			topList <- c(topList, topic)
		}
		res <- append(res, list(topList))
		names(res[[i]]) <- NULL
		base = base + length(doc)
		i = i + 1
	}
	res
}

assignToTopics <- function(lda, k=10, file=FALSE) {
	procDocsDFM <- preProcessDocs()
	procDocs <- posterior(lda, procDocsDFM)$topics
	newTerms <- compareDictionaries(lda, procDocsDFM)	
	nDocs <- get("count")
	dictLog <- "newTerms.txt"
	res <- list()
	write("", file=dictLog)
	cat("Check", dictLog, "for uncovered terms\n")
	i = 1
	base = 0
	while (i <= nDocs) {
		cat("Processing sequence", i, "\n")
		i <- as.integer(i)
		if (file) {
			outAttr <- select("file", i)
		} else {
			outAttr <- select("document", i)
		}
		threadSeq <- select("thread", i)
		typeSeq <- select("type", i)
		timeSeq <- select("timestamp", i)
		tops <- vector("list", k)
		for (j in seq(outAttr)) {
			eventDist <- procDocs[base + j, ]
			topic <- which.max(eventDist)
			elem <- NULL
			elem$dist <- eventDist
			elem$event <- outAttr[[j]]
			# Mark events with new vocaulary
			if (sum(procDocsDFM[base + j, newTerms]) > 0) {
				data <- paste("Mapped to", topic, "{", elem$event, "}")
				write(data, append=TRUE, file=dictLog)
			}
			new <- list(elem)
			names(new) <- paste(typeSeq[[j]], timeSeq[[j]], threadSeq[[j]])
			tops[[topic]] <- append(tops[[topic]], new)
		}
		res <- append(res, list(tops))
		base = base + length(outAttr)
		i = i + 1
	}
	res
}

countAssignments <- function(logMap) {
	res <- vector(, length(logMap[[1]]))
	for (i in seq(logMap)) {
		for (j in seq(logMap[[i]])) {
			res[[j]] <- res[[j]] + length(logMap[[i]][[j]])
		}
	}
	res
}

